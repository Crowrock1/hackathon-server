const users = [];

const addUser = ({ id, name, room, isTeacher }) => {
  name = name.trim();
  room = room.trim();

  const existingUser = users.find((user) => user.room.toLowerCase() === room.toLowerCase() && user.name.toLowerCase() === name.toLowerCase());

  if(!name || !room) return { error: 'Username and room are required.' };
  if(existingUser) return { error: 'Username is taken.' };

  const user = { id, name, room, isTeacher };

  users.push(user);

  return { user };
}

const removeUser = (id) => {
  const index = users.findIndex((user) => user.id === id);

  if(index !== -1) return users.splice(index, 1)[0];
}

const getTeacher = () => users.filter((user) => user.isTeacher);

const getUserByID = (id) => users.find((user) => user.id === id);

const getUsersInRoom = (room) => users.filter((user) => user.room.toLowerCase() === room.toLowerCase());

module.exports = { addUser, removeUser, getUserByID, getTeacher, getUsersInRoom };