const data = require('./whitelist.json');
const http = require('http');
const express = require('express');
const socketio = require('socket.io');
const cors = require('cors');

const { addUser, removeUser, getUserByID, getUsersInRoom, getTeacher } = require('./users');

const router = require('./router');

const app = express();
const server = http.createServer(app);
const io = socketio(server);

app.use(cors());
app.use(router);

io.on('connect', (socket) => {
  socket.on('join', ({ name, room }, callback) => {
    const teachers = [];
    data.forEach((item) => {
      teachers.push(item.name);
    });
  
    const { error, user } = addUser({ id: socket.id, name, room, isTeacher: teachers.includes(name) });

    if(error) return callback(error);

    socket.join(user.room);

    socket.broadcast.to(user.room).emit('message', { user: 'admin', text: `${user.name} has joined!` });

    io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room) });

    callback();
  });

  socket.on('sendMessage', (message, callback) => {
    const user = getUserByID(socket.id);
    const teachers = [];
    const currUsers = getUsersInRoom(user.room);
    data.forEach((item) => {
      teachers.push(item.name);
    });

    if (user.isTeacher) {
      socket.broadcast.to(user.room).emit('message', { user: user.name, text: message});
      io.to(user.id).emit('message', { user: user.name, text: message});
    } else {
      
      const teacher = getTeacher();
      
      teacher.forEach((item) => {
        io.to(item.id).emit('message', { user: user.name, text: message });

      });


      io.to(teacher.id).emit('message', { user: user.name, text: message });
    }

    callback();
  });

  socket.on('disconnect', () => {
    const user = removeUser(socket.id);

    if(user) {
      io.to(user.room).emit('message', { user: 'admin', text: `${user.name} has left.` });
      io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room)});
    }
  })
});

server.listen(process.env.PORT || 5000, () => console.log(`Server has started.`));